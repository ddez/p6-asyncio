import asyncio
import sdp_transform
import json
sdp_doc = sdp_transform.parse(open("doc_answer.sdp").read())
sdp_answer = sdp_transform.write(sdp_doc)

message = {
    "type": "answer",
    "sdp": sdp_answer
}
message = json.dumps(message)
class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, message, addr):
        message = sdp_answer.encode()
        print('Received %r from %s' % (message, addr))
        print('Send %r to %s' % (message, addr))
        self.transport.sendto(message, addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())