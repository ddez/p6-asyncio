import asyncio
import random


async def counter(id, time):
    print(f'Counter {id} started')
    await asyncio.sleep(1)

    for i in range(time):
        print(f'Counter {id} - {i + 1}')
        if i + 1 == time:
            print(f'Counter {id} finished')
        await asyncio.sleep(1)



async def main():
    # tasks = []
    #
    # for i in range(3):
    #     task = asyncio.create_task(counter(i, random.randint(1, 5)))
    #     tasks.append(task)

    await asyncio.gather(counter('A',4),counter('B',2),counter ('C',6))


asyncio.run(main())
