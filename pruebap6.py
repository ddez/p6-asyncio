import asyncio

async def main():
    print('¡Hola ...')
    await asyncio.sleep(1)
    print('... mundo!')

asyncio.run(main())
